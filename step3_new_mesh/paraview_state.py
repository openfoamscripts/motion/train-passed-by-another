# state file generated using paraview version 5.11.2
import paraview
paraview.compatibility.major = 5
paraview.compatibility.minor = 11

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# Create a new 'Line Chart View'
lineChartView1 = CreateView('XYChartView')
lineChartView1.ViewSize = [482, 567]
lineChartView1.LegendPosition = [405, 525]
lineChartView1.LeftAxisUseCustomRange = 1
lineChartView1.LeftAxisRangeMaximum = 170.0
lineChartView1.BottomAxisUseCustomRange = 1
lineChartView1.BottomAxisRangeMinimum = -15.0
lineChartView1.BottomAxisRangeMaximum = 15.0
lineChartView1.RightAxisRangeMaximum = 6.66
lineChartView1.TopAxisRangeMaximum = 6.66

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1108, 567]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [1.0, 0.0, 5.000000149011612]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [1.0, 0.0, 248.7136960371762]
renderView1.CameraFocalPoint = [1.0, 0.0, 5.000000149011612]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 24.31920170121629
renderView1.CameraParallelProjection = 1

# init the 'GridAxes3DActor' selected for 'AxesGrid'
renderView1.AxesGrid.Visibility = 1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.SplitHorizontal(0, 0.694894)
layout1.AssignView(1, renderView1)
layout1.AssignView(2, lineChartView1)
layout1.SetSize(1591, 567)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
step3_new_meshfoam = OpenFOAMReader(registrationName='step3_new_mesh.foam', FileName='/mnt/HDD1/GitLab/OF/motion/train-passed-by-another/step3_new_mesh/step3_new_mesh.foam')
step3_new_meshfoam.MeshRegions = ['internalMesh']
step3_new_meshfoam.CellArrays = ['U', 'cellTypes', 'nuTilda', 'nut', 'p', 'zoneID']

# create a new 'Threshold'
threshold1 = Threshold(registrationName='Threshold1', Input=step3_new_meshfoam)
threshold1.Scalars = ['CELLS', 'cellTypes']
threshold1.LowerThreshold = 1.0
threshold1.UpperThreshold = 5884.94287109375
threshold1.ThresholdMethod = 'Below Lower Threshold'

# create a new 'Glyph'
glyph1 = Glyph(registrationName='Glyph1', Input=threshold1,
    GlyphType='Arrow')
glyph1.OrientationArray = ['POINTS', 'U']
glyph1.ScaleArray = ['POINTS', 'No scale array']
glyph1.ScaleFactor = 2.0
glyph1.GlyphTransform = 'Transform2'

# create a new 'Plot Over Line'
plotOverLine1 = PlotOverLine(registrationName='PlotOverLine1', Input=threshold1)
plotOverLine1.Resolution = 60
plotOverLine1.Point1 = [0.0, -15.0, 10.0]
plotOverLine1.Point2 = [0.0, 15.0, 10.0]

# create a new 'Annotate Time Filter'
annotateTimeFilter1 = AnnotateTimeFilter(registrationName='AnnotateTimeFilter1', Input=threshold1)

# ----------------------------------------------------------------
# setup the visualization in view 'lineChartView1'
# ----------------------------------------------------------------

# show data from plotOverLine1
plotOverLine1Display = Show(plotOverLine1, lineChartView1, 'XYChartRepresentation')

# trace defaults for the display properties.
plotOverLine1Display.UseIndexForXAxis = 0
plotOverLine1Display.XArrayName = 'Points_Y'
plotOverLine1Display.SeriesVisibility = ['U_X']
plotOverLine1Display.SeriesLabel = ['arc_length', 'arc_length', 'cellTypes', 'cellTypes', 'nut', 'nut', 'nuTilda', 'nuTilda', 'p', 'p', 'U_X', 'U_X', 'U_Y', 'U_Y', 'U_Z', 'U_Z', 'U_Magnitude', 'U_Magnitude', 'vtkValidPointMask', 'vtkValidPointMask', 'zoneID', 'zoneID', 'Points_X', 'Points_X', 'Points_Y', 'Points_Y', 'Points_Z', 'Points_Z', 'Points_Magnitude', 'Points_Magnitude']
plotOverLine1Display.SeriesColor = ['arc_length', '0', '0', '0', 'cellTypes', '0.8899977111467154', '0.10000762951094835', '0.1100022888532845', 'nut', '0.220004577706569', '0.4899977111467155', '0.7199969481956207', 'nuTilda', '0.30000762951094834', '0.6899977111467155', '0.2899977111467155', 'p', '0.6', '0.3100022888532845', '0.6399938963912413', 'U_X', '1', '0.5000076295109483', '0', 'U_Y', '0.6500038147554742', '0.3400015259021897', '0.16000610360875867', 'U_Z', '0', '0', '0', 'U_Magnitude', '0.8899977111467154', '0.10000762951094835', '0.1100022888532845', 'vtkValidPointMask', '0.220004577706569', '0.4899977111467155', '0.7199969481956207', 'zoneID', '0.30000762951094834', '0.6899977111467155', '0.2899977111467155', 'Points_X', '0.6', '0.3100022888532845', '0.6399938963912413', 'Points_Y', '1', '0.5000076295109483', '0', 'Points_Z', '0.6500038147554742', '0.3400015259021897', '0.16000610360875867', 'Points_Magnitude', '0', '0', '0']
plotOverLine1Display.SeriesOpacity = ['arc_length', '1', 'cellTypes', '1', 'nut', '1', 'nuTilda', '1', 'p', '1', 'U_X', '1', 'U_Y', '1', 'U_Z', '1', 'U_Magnitude', '1', 'vtkValidPointMask', '1', 'zoneID', '1', 'Points_X', '1', 'Points_Y', '1', 'Points_Z', '1', 'Points_Magnitude', '1']
plotOverLine1Display.SeriesPlotCorner = ['Points_Magnitude', '0', 'Points_X', '0', 'Points_Y', '0', 'Points_Z', '0', 'U_Magnitude', '0', 'U_X', '0', 'U_Y', '0', 'U_Z', '0', 'arc_length', '0', 'cellTypes', '0', 'nuTilda', '0', 'nut', '0', 'p', '0', 'vtkValidPointMask', '0', 'zoneID', '0']
plotOverLine1Display.SeriesLabelPrefix = ''
plotOverLine1Display.SeriesLineStyle = ['Points_Magnitude', '1', 'Points_X', '1', 'Points_Y', '1', 'Points_Z', '1', 'U_Magnitude', '1', 'U_X', '1', 'U_Y', '1', 'U_Z', '1', 'arc_length', '1', 'cellTypes', '1', 'nuTilda', '1', 'nut', '1', 'p', '1', 'vtkValidPointMask', '1', 'zoneID', '1']
plotOverLine1Display.SeriesLineThickness = ['Points_Magnitude', '2', 'Points_X', '2', 'Points_Y', '2', 'Points_Z', '2', 'U_Magnitude', '2', 'U_X', '2', 'U_Y', '2', 'U_Z', '2', 'arc_length', '2', 'cellTypes', '2', 'nuTilda', '2', 'nut', '2', 'p', '2', 'vtkValidPointMask', '2', 'zoneID', '2']
plotOverLine1Display.SeriesMarkerStyle = ['Points_Magnitude', '0', 'Points_X', '0', 'Points_Y', '0', 'Points_Z', '0', 'U_Magnitude', '0', 'U_X', '0', 'U_Y', '0', 'U_Z', '0', 'arc_length', '0', 'cellTypes', '0', 'nuTilda', '0', 'nut', '0', 'p', '0', 'vtkValidPointMask', '0', 'zoneID', '0']
plotOverLine1Display.SeriesMarkerSize = ['Points_Magnitude', '4', 'Points_X', '4', 'Points_Y', '4', 'Points_Z', '4', 'U_Magnitude', '4', 'U_X', '4', 'U_Y', '4', 'U_Z', '4', 'arc_length', '4', 'cellTypes', '4', 'nuTilda', '4', 'nut', '4', 'p', '4', 'vtkValidPointMask', '4', 'zoneID', '4']

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from threshold1
threshold1Display = Show(threshold1, renderView1, 'UnstructuredGridRepresentation')

# get 2D transfer function for 'U'
uTF2D = GetTransferFunction2D('U')
uTF2D.ScalarRangeInitialized = 1
uTF2D.Range = [0.0, 160.0, 0.0, 1.0]

# get color transfer function/color map for 'U'
uLUT = GetColorTransferFunction('U')
uLUT.AutomaticRescaleRangeMode = 'Never'
uLUT.TransferFunction2D = uTF2D
uLUT.RGBPoints = [0.0, 0.0, 0.0, 1.0, 160.0, 1.0, 0.0, 0.0]
uLUT.ColorSpace = 'HSV'
uLUT.NanColor = [0.498039215686, 0.498039215686, 0.498039215686]
uLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'U'
uPWF = GetOpacityTransferFunction('U')
uPWF.Points = [0.0, 0.0, 0.5, 0.0, 160.0, 1.0, 0.5, 0.0]
uPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
threshold1Display.Representation = 'Surface'
threshold1Display.ColorArrayName = ['CELLS', 'U']
threshold1Display.LookupTable = uLUT
threshold1Display.SelectTCoordArray = 'None'
threshold1Display.SelectNormalArray = 'None'
threshold1Display.SelectTangentArray = 'None'
threshold1Display.OSPRayScaleArray = 'p'
threshold1Display.OSPRayScaleFunction = 'PiecewiseFunction'
threshold1Display.SelectOrientationVectors = 'U'
threshold1Display.ScaleFactor = 12.0
threshold1Display.SelectScaleArray = 'p'
threshold1Display.GlyphType = 'Arrow'
threshold1Display.GlyphTableIndexArray = 'p'
threshold1Display.GaussianRadius = 0.6
threshold1Display.SetScaleArray = ['POINTS', 'p']
threshold1Display.ScaleTransferFunction = 'PiecewiseFunction'
threshold1Display.OpacityArray = ['POINTS', 'p']
threshold1Display.OpacityTransferFunction = 'PiecewiseFunction'
threshold1Display.DataAxesGrid = 'GridAxesRepresentation'
threshold1Display.PolarAxes = 'PolarAxesRepresentation'
threshold1Display.ScalarOpacityFunction = uPWF
threshold1Display.ScalarOpacityUnitDistance = 7.573142606741618
threshold1Display.OpacityArrayName = ['POINTS', 'p']
threshold1Display.SelectInputVectors = ['POINTS', 'U']
threshold1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
threshold1Display.ScaleTransferFunction.Points = [-6610.81298828125, 0.0, 0.5, 0.0, 5884.94287109375, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
threshold1Display.OpacityTransferFunction.Points = [-6610.81298828125, 0.0, 0.5, 0.0, 5884.94287109375, 1.0, 0.5, 0.0]

# show data from annotateTimeFilter1
annotateTimeFilter1Display = Show(annotateTimeFilter1, renderView1, 'TextSourceRepresentation')

# trace defaults for the display properties.
annotateTimeFilter1Display.FontSize = 28

# show data from glyph1
glyph1Display = Show(glyph1, renderView1, 'GeometryRepresentation')

# get 2D transfer function for 'p'
pTF2D = GetTransferFunction2D('p')

# get color transfer function/color map for 'p'
pLUT = GetColorTransferFunction('p')
pLUT.TransferFunction2D = pTF2D
pLUT.RGBPoints = [-32689.544921875, 0.231373, 0.298039, 0.752941, -11595.7138671875, 0.865003, 0.865003, 0.865003, 9498.1171875, 0.705882, 0.0156863, 0.14902]
pLUT.ScalarRangeInitialized = 1.0

# trace defaults for the display properties.
glyph1Display.Representation = 'Surface'
glyph1Display.ColorArrayName = ['POINTS', '']
glyph1Display.LookupTable = pLUT
glyph1Display.SelectTCoordArray = 'None'
glyph1Display.SelectNormalArray = 'None'
glyph1Display.SelectTangentArray = 'None'
glyph1Display.OSPRayScaleArray = 'p'
glyph1Display.OSPRayScaleFunction = 'PiecewiseFunction'
glyph1Display.SelectOrientationVectors = 'U'
glyph1Display.ScaleFactor = 204.74331407546998
glyph1Display.SelectScaleArray = 'p'
glyph1Display.GlyphType = 'Arrow'
glyph1Display.GlyphTableIndexArray = 'p'
glyph1Display.GaussianRadius = 10.237165703773499
glyph1Display.SetScaleArray = ['POINTS', 'p']
glyph1Display.ScaleTransferFunction = 'PiecewiseFunction'
glyph1Display.OpacityArray = ['POINTS', 'p']
glyph1Display.OpacityTransferFunction = 'PiecewiseFunction'
glyph1Display.DataAxesGrid = 'GridAxesRepresentation'
glyph1Display.PolarAxes = 'PolarAxesRepresentation'
glyph1Display.SelectInputVectors = ['POINTS', 'U']
glyph1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
glyph1Display.ScaleTransferFunction.Points = [-37405.60546875, 0.0, 0.5, 0.0, -148.37716674804688, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
glyph1Display.OpacityTransferFunction.Points = [-37405.60546875, 0.0, 0.5, 0.0, -148.37716674804688, 1.0, 0.5, 0.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for uLUT in view renderView1
uLUTColorBar = GetScalarBar(uLUT, renderView1)
uLUTColorBar.Orientation = 'Horizontal'
uLUTColorBar.WindowLocation = 'Any Location'
uLUTColorBar.Position = [0.6143817689530684, 0.0254320987654321]
uLUTColorBar.Title = 'U'
uLUTColorBar.ComponentTitle = 'Magnitude'
uLUTColorBar.TitleFontSize = 24
uLUTColorBar.LabelFontSize = 24
uLUTColorBar.ScalarBarLength = 0.33000000000000007

# set color bar visibility
uLUTColorBar.Visibility = 1

# show color legend
threshold1Display.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get opacity transfer function/opacity map for 'p'
pPWF = GetOpacityTransferFunction('p')
pPWF.Points = [-32689.544921875, 0.0, 0.5, 0.0, 9498.1171875, 1.0, 0.5, 0.0]
pPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# restore active source
SetActiveSource(threshold1)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')