# チュートリアルの改変

Author: M.TANAKA  
Created: 2024/01/20  
Modified: 2024/01/20  

[TOC]

## 概要

既存のblockMeshDictのパラメータを弄るのではなく，メッシュを新規に作成する．

### 問題

oversetメッシュの計算において，計算されるべきセル（cellTypes=0:Culculated）も計算不要なセル(cellTypes=2:Hole)とみなされてしまう．

![](./animation/mesh.png)

生成したメッシュ

![](./animation/step3_flow.gif)

計算結果（cellTypes=0,1のみを表示）

![](../step2_rotatingSquare_mod/animation/step2_flow.gif)

※こうであってほしい(../step2_rotatingSquare_mod/animation/step2_flow.gif)

参考：
- http://www.wolfdynamics.com/training/movingbodies/OF2021/dynamicmeshes_2021_OF8.pdf
- 上記PDFの117ページ

## やったこと

###メッシュ生成

単純な形状のメッシュをChatGPTに作ってもらい，そこから修正していく作戦．

<details>
  <summary>blockMeshDict作って！</summary>

>私はOpenFOAMv2312を使用しています．以下のようなメッシュを作成するblockMeshDictを作成してください．足りない情報は聞き返してください．
>- 2次元領域．z方向には1セルのみ，zは0~10mの範囲とする．
>- xy平面内には2つの同心円の間のドーナツ上の領域を考える．
>    - 内側の円の半径は2m，外側の円の半径は5m．
>    - セルの分割は，半径方向（２つの円の間）を10分割，円周方向に36分割（つまり10度ごとに1セル）とする．
>- 境界の名前は，内側の円筒をwalls，外側の円筒をoversetPatch，残るz平面に並行な面たちをfrontAndBackとする．
>    - パッチタイプは，wallsがwall，oversetPatchがoverset，frontAndBackがemptyである．

---

>指定された要件に基づいて、以下のようなblockMeshDictファイルを作成しました。
>```cpp
>FoamFile
>{
>    version     2.0;
>    format      ascii;
>    class       dictionary;
>    object      blockMeshDict;
>}
>// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
>
>convertToMeters 1;
>
>vertices
>(
>    (-2 0 0)    // 0
>    (2 0 0)     // 1
>    (0 -2 0)    // 2
>    (0 2 0)     // 3
>    (-5 0 0)    // 4
>    (5 0 0)     // 5
>    (0 -5 0)    // 6
>    (0 5 0)     // 7
>    (-2 0 10)   // 8
>    (2 0 10)    // 9
>    (0 -2 10)   // 10
>    (0 2 10)    // 11
>    (-5 0 10)   // 12
>    (5 0 10)    // 13
>    (0 -5 10)   // 14
>    (0 5 10)    // 15
>);
>
>blocks
>(
>    hex (0 1 3 2 8 9 11 10) (10 36 1) simpleGrading (1 1 1)
>    hex (0 1 5 4 8 9 13 12) (10 36 1) simpleGrading (1 1 1)
>);
>
>edges
>(
>    arc 0 1 (1.41421356 0 0)
>    arc 1 3 (0 1.41421356 0)
>    arc 3 2 (-1.41421356 0 0)
>    arc 2 0 (0 -1.41421356 0)
>    arc 4 5 (3.53553391 0 0)
>    arc 5 7 (0 3.53553391 0)
>    arc 7 6 (-3.53553391 0 0)
>    arc 6 4 (0 -3.53553391 0)
>    arc 8 9 (1.41421356 0 10)
>    arc 9 11 (0 1.41421356 10)
>    arc 11 10 (-1.41421356 0 10)
>    arc 10 8 (0 -1.41421356 10)
>    arc 12 13 (3.53553391 0 10)
>    arc 13 15 (0 3.53553391 10)
>    arc 15 14 (-3.53553391 0 10)
>    arc 14 12 (0 -3.53553391 10)
>);
>
>boundary
>(
>    walls
>    {
>        type wall;
>        faces
>        (
>            (0 1 9 8)
>            (1 3 11 9)
>            (3 2 10 11)
>            (2 0 8 10)
>        );
>    }
>    oversetPatch
>    {
>        type patch;
>        faces
>        (
>            (0 4 12 8)
>            (4 5 13 12)
>            (5 1 9 13)
>            (1 0 8 9)
>        );
>    }
>    frontAndBack
>    {
>        type empty;
>        faces
>        (
>            (0 1 5 4)
>            (1 3 7 5)
>            (3 2 6 7)
>            (2 0 4 6)
>            (8 9 13 12)
>            (9 11 15 13)
>            (11 10 14 15)
>            (10 8 12 14)
>        );
>    }
>);
>
>mergePatchPairs
>(
>);
>
>// ************************************************************************* //
>```
>このファイルは、指定された要件に基づいて作成されていますが、必要に応じて調整してください。また、このファイルを使用する前に、OpenFOAMのblockMeshユーティリティを使用してメッシュを生成し、その結果を確認してください。このファイルは、指定された要件に基づいて作成されていますが、必要に応じて調整してください。特に、セルの分割数やパッチのタイプなど、具体的な要件に合わせて調整が必要な場合があります。
>
>最後に、このblockMeshDictファイルを使用する前に、OpenFOAMのblockMeshユーティリティを使用してメッシュを生成し、その結果を確認してください。これにより、生成されたメッシュが要件を満たしているかどうかを確認することができます。

</details>

すごぶるそれっぽいけど，それぞれの記述に整合性はない．実行するとこうなった：

```
Creating block mesh from "system/blockMeshDict"
--> FOAM IOWarning :
    Found [v1012] 'convertToMeters' entry instead of 'scale' in dictionary "system/blockMeshDict"

    This keyword is 156 months old.

Creating block edges


--> FOAM FATAL ERROR: (openfoam-2312)
0

    From void Foam::blockEdges::arcEdge::calcFromMidPoint(const point&, const point&, const point&)
    in file blockEdges/arcEdge/arcEdge.C at line 66.

FOAM aborting
```

結局，手動で作成することにした．

## 境界条件

step2からほとんど変更なし．

- 全圧入口，速度出口としたらどうか？

## 遭遇したエラー

### 必要なセルが無視される

- メッシュを止めてみる．

- 背景メッシュのセルより移動メッシュのセルを大きくしたら直った．

- nuTildaの境界条件の設定

```cpp
C_mu            0.09;
U_in            80;
I               0.05;
L               4;
nuTilda_in      #eval{sqrt(1.5) * $C_mu * $U_in * $I * $L};
```