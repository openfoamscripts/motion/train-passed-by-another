# state file generated using paraview version 5.11.2
import os
import paraview
paraview.compatibility.major = 5
paraview.compatibility.minor = 11

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [946, 567]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [0.005, 0.004, 0.005]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [0.005, 0.004, 0.04]
renderView1.CameraFocalPoint = [0.005, 0.004, 0.005]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 0.007679999999999999
renderView1.CameraParallelProjection = 1

# init the 'GridAxes3DActor' selected for 'AxesGrid'
renderView1.AxesGrid.Visibility = 1
renderView1.AxesGrid.ShowGrid = 1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(946, 567)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
dir = os.path.dirname(__file__)
rotatingSquarefoam = OpenFOAMReader(registrationName='rotatingSquare.foam', FileName=f'{dir}/rotatingSquare.foam')
rotatingSquarefoam.MeshRegions = ['patch/frontAndBack']
rotatingSquarefoam.CellArrays = ['U', 'cellTypes', 'p', 'zoneID', 'epsilon', 'k', 'nuTilda', 'nut']
rotatingSquarefoam.PointArrays = ['pointDisplacement']

# create a new 'Glyph'
glyph1 = Glyph(registrationName='Glyph1', Input=rotatingSquarefoam,
    GlyphType='Arrow')
glyph1.OrientationArray = ['CELLS', 'U']
glyph1.ScaleArray = ['CELLS', 'U']
glyph1.ScaleFactor = 0.00075
glyph1.GlyphTransform = 'Transform2'

# create a new 'Annotate Time Filter'
annotateTimeFilter1 = AnnotateTimeFilter(registrationName='AnnotateTimeFilter1', Input=rotatingSquarefoam)

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from rotatingSquarefoam
rotatingSquarefoamDisplay = Show(rotatingSquarefoam, renderView1, 'UnstructuredGridRepresentation')

# get 2D transfer function for 'U'
uTF2D = GetTransferFunction2D('U')
uTF2D.ScalarRangeInitialized = 1
uTF2D.Range = [0.0, 2.0, 0.0, 1.0]

# get color transfer function/color map for 'U'
uLUT = GetColorTransferFunction('U')
uLUT.AutomaticRescaleRangeMode = 'Never'
uLUT.TransferFunction2D = uTF2D
uLUT.RGBPoints = [0.0, 0.0, 0.0, 1.0, 2.0, 1.0, 0.0, 0.0]
uLUT.ColorSpace = 'HSV'
uLUT.NanColor = [0.5, 0.5, 0.5]
uLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'U'
uPWF = GetOpacityTransferFunction('U')
uPWF.Points = [0.0, 0.0, 0.5, 0.0, 2.0, 1.0, 0.5, 0.0]
uPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
rotatingSquarefoamDisplay.Representation = 'Surface'
rotatingSquarefoamDisplay.ColorArrayName = ['CELLS', 'U']
rotatingSquarefoamDisplay.LookupTable = uLUT
rotatingSquarefoamDisplay.SelectTCoordArray = 'None'
rotatingSquarefoamDisplay.SelectNormalArray = 'None'
rotatingSquarefoamDisplay.SelectTangentArray = 'None'
rotatingSquarefoamDisplay.OSPRayScaleArray = 'p'
rotatingSquarefoamDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
rotatingSquarefoamDisplay.SelectOrientationVectors = 'U'
rotatingSquarefoamDisplay.ScaleFactor = 0.0012
rotatingSquarefoamDisplay.SelectScaleArray = 'p'
rotatingSquarefoamDisplay.GlyphType = 'Arrow'
rotatingSquarefoamDisplay.GlyphTableIndexArray = 'p'
rotatingSquarefoamDisplay.GaussianRadius = 6e-05
rotatingSquarefoamDisplay.SetScaleArray = ['POINTS', 'p']
rotatingSquarefoamDisplay.ScaleTransferFunction = 'PiecewiseFunction'
rotatingSquarefoamDisplay.OpacityArray = ['POINTS', 'p']
rotatingSquarefoamDisplay.OpacityTransferFunction = 'PiecewiseFunction'
rotatingSquarefoamDisplay.DataAxesGrid = 'GridAxesRepresentation'
rotatingSquarefoamDisplay.PolarAxes = 'PolarAxesRepresentation'
rotatingSquarefoamDisplay.ScalarOpacityFunction = uPWF
rotatingSquarefoamDisplay.ScalarOpacityUnitDistance = 0.00194
rotatingSquarefoamDisplay.OpacityArrayName = ['POINTS', 'p']
rotatingSquarefoamDisplay.SelectInputVectors = ['POINTS', 'U']
rotatingSquarefoamDisplay.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
rotatingSquarefoamDisplay.ScaleTransferFunction.Points = [-2.51, 0.0, 0.5, 0.0, 1.84, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
rotatingSquarefoamDisplay.OpacityTransferFunction.Points = [-2.51, 0.0, 0.5, 0.0, 1.84, 1.0, 0.5, 0.0]

# show data from glyph1
glyph1Display = Show(glyph1, renderView1, 'GeometryRepresentation')

# get 2D transfer function for 'p'
pTF2D = GetTransferFunction2D('p')

# get color transfer function/color map for 'p'
pLUT = GetColorTransferFunction('p')
pLUT.TransferFunction2D = pTF2D
pLUT.RGBPoints = [-2.514857769012451, 0.231373, 0.298039, 0.752941, -0.3377091884613037, 0.865003, 0.865003, 0.865003, 1.8394393920898438, 0.705882, 0.0156863, 0.14902]
pLUT.ScalarRangeInitialized = 1.0

# trace defaults for the display properties.
glyph1Display.Representation = 'Surface'
glyph1Display.ColorArrayName = ['POINTS', '']
glyph1Display.LookupTable = pLUT
glyph1Display.SelectTCoordArray = 'None'
glyph1Display.SelectNormalArray = 'None'
glyph1Display.SelectTangentArray = 'None'
glyph1Display.OSPRayScaleArray = 'p'
glyph1Display.OSPRayScaleFunction = 'PiecewiseFunction'
glyph1Display.SelectOrientationVectors = 'U'
glyph1Display.ScaleFactor = 0.00118
glyph1Display.SelectScaleArray = 'p'
glyph1Display.GlyphType = 'Arrow'
glyph1Display.GlyphTableIndexArray = 'p'
glyph1Display.GaussianRadius = 5.92e-05
glyph1Display.SetScaleArray = ['POINTS', 'p']
glyph1Display.ScaleTransferFunction = 'PiecewiseFunction'
glyph1Display.OpacityArray = ['POINTS', 'p']
glyph1Display.OpacityTransferFunction = 'PiecewiseFunction'
glyph1Display.DataAxesGrid = 'GridAxesRepresentation'
glyph1Display.PolarAxes = 'PolarAxesRepresentation'
glyph1Display.SelectInputVectors = ['POINTS', 'U']
glyph1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
glyph1Display.ScaleTransferFunction.Points = [-1.63, 0.0, 0.5, 0.0, 1.84, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
glyph1Display.OpacityTransferFunction.Points = [-1.63, 0.0, 0.5, 0.0, 1.84, 1.0, 0.5, 0.0]

# show data from annotateTimeFilter1
annotateTimeFilter1Display = Show(annotateTimeFilter1, renderView1, 'TextSourceRepresentation')

# trace defaults for the display properties.
annotateTimeFilter1Display.FontSize = 32

# setup the color legend parameters for each legend in this view

# get color legend/bar for uLUT in view renderView1
uLUTColorBar = GetScalarBar(uLUT, renderView1)
uLUTColorBar.Position = [0.8786610878661087, 0.01646090534979424]
uLUTColorBar.Title = 'U'
uLUTColorBar.ComponentTitle = 'Magnitude'
uLUTColorBar.TitleFontSize = 24
uLUTColorBar.LabelFontSize = 24

# set color bar visibility
uLUTColorBar.Visibility = 1

# show color legend
rotatingSquarefoamDisplay.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get opacity transfer function/opacity map for 'p'
pPWF = GetOpacityTransferFunction('p')
pPWF.Points = [-2.514857769012451, 0.0, 0.5, 0.0, 1.8394393920898438, 1.0, 0.5, 0.0]
pPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# restore active source
SetActiveSource(rotatingSquarefoam)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')