# チュートリアルの読み解き

Author: M.TANAKA  
Created: 2024/01/20  
Modified: 2024/01/20  

[TOC]

## 概要

今回解析したいケースは，一から設定するには筆者に知識が足りない．まずは類似するチュートリアルケースを読み解くことから始める．

なお，計算結果の可視化動画は[ここ](animation/)に保存している．

## 計算の流れ

`Allrun.pre`を見ると，こうある．順に見ていく．

```sh
#!/bin/sh
cd "${0%/*}" || exit                                # Run from this directory
. ${WM_PROJECT_DIR:?}/bin/tools/RunFunctions        # Tutorial run functions
#------------------------------------------------------------------------------

runApplication blockMesh

# Select cellSets
runApplication topoSet

runApplication subsetMesh box -patch hole -overwrite

# Select cellSets
runApplication -s box topoSet

restore0Dir

# Use cellSets to write zoneID
runApplication setFields

#------------------------------------------------------------------------------
```

## blockMesh

`blockMeshDict`で何をしているのか，Cursorに聞いてみた．

### gpt-3.5 turbo

<details>
  <summary>クリックして展開/折りたたみ</summary>

>(ファイル内容をadd to chatした上で)
>@Codebase @OFRepo @OFDoc2312 @OFCodeWiki @OFTutoWiki @OFDocs @OFPrimer @OFNotes @OFHoltzman このblockMeshDictでは，どのようなメッシュを生成しているのか，できるだけ詳しく説明してください．

---

>```sh
>runApplication blockMesh
>```
>この行は、blockMeshというメッシュ生成アプリケーションを実行しています。blockMeshは、定義された頂点と境界条件に基づいて、六面体ブロックメッシュを生成します。このメッシュは、定義された頂点と面によって定義され、内部のセルも含まれます。メッシュは、流体力学シミュレーションに使用されます。

</details>

@***は自分でCursorに登録したドキュメント．
- 追々設定メモを別プロジェクトに残しておきたい．

ふわっとした回答．このチュートリアルにフォーカスした回答が欲しい．GPT4でもう一度聞く．

### gpt-4

<details>
  <summary>クリックして展開/折りたたみ</summary>
  
>@Codebase @OFRepo @OFDoc2312 @OFCodeWiki @OFTutoWiki @OFDocs @OFPrimer @OFNotes @OFHoltzman このblockMeshDictでは，どんなメッシュを生成しようとしているのか，できるだけ詳しく教えてください．

---

>このblockMeshDictファイルは、2つの六面体（hexahedral）ブロックからなるメッシュを生成するための設定を含んでいます。scaleパラメータは、メッシュのスケールを定義しており、ここでは0.01に設定されています。これは、すべての座標値が実際の寸法の0.01倍であることを意味します。
>
>verticesセクションでは、メッシュの頂点が定義されています。最初の8つの頂点（0から7）は、大きな背景ブロックを定義するために使用され、次の8つの頂点（8から15）は、移動するゾーンを定義するために使用されます。
>
>blocksセクションでは、2つのブロックが定義されています。最初のhexブロックは背景ブロックで、25x25x1のセルで構成されています。2番目のhexブロックは移動ゾーンで、16x24x1のセルで構成されています。両方のブロックはsimpleGradingを使用して均一なセルサイズでグレーディングされています。
>
>boundaryセクションでは、メッシュの境界条件が定義されています。walls、inlet、outlet、frontAndBack、およびholeという名前のパッチがあります。holeパッチはsubsetMeshによって後で埋められるため、ここでは空のフェースリストが与えられています。
>
>frontAndBackパッチはtype emptyとして定義されており、これは2次元のメッシュを示しています。このタイプは、メッシュが2Dであることを示し、z方向には厚みがないことを意味します。
>
>この設定により、2Dの背景メッシュとその中に埋め込まれた移動ゾーンが生成されます。これは、回転する正方形などの動的なオブジェクトをシミュレートするために使用される可能性があります。

</details>

期待していた粒度の回答が得られた．OpenFOAMのようなマイナーなツールに関しては，GPT4は必須と思われる．月20ドルは必要な投資か．


## topoSet

`runApplication topoSet`で何をしているのか，Cursorに聞いてみた．

### gpt-3.5 turbo

<details>
  <summary>クリックして展開/折りたたみ</summary>

>(`topoSetDict`をadd to chatして)
>@Codebase @OFRepo @OFDoc2312 @OFCodeWiki @OFTutoWiki @OFDocs @OFPrimer @OFNotes @OFHoltzman このtopoSetDictではメッシュにどんな変更を加えようとしているのか，できるだけ詳しく教えてください．
>
---

>```cpp
>actions
>(
>    {
>        name    c0;
>        type    cellSet;
>        action  new;
>        source  cylinderToCell;
>        sourceInfo
>        {
>            cylinderToCell
>            {
>                cylinder    cylinder;
>                tolerance   1e-06;
>            }
>        }
>    }
>);
>```
>[1](https://doc.openfoam.com/2312/quickstart/)にアクセスすると、OpenFOAMのメッシュ生成に関する基本的な情報が得られます。しかし、オーバーセットメッシュの具体的な設定については、このドキュメントでは直接的な情報は提供されていません。 オーバーセットメッシュの設定に関しては、通常、以下のようなファイルや設定が関与します。
>
>- dynamicMeshDict: メッシュの動的な動きを定義する設定ファイルです。オーバーセットメッシュのコンテキストでは、このファイル内でメッシュ間の相対的な動きや、メッシュの更新方法を指定します。
>- meshModifiers: オーバーセットメッシュの設定を含むことがあるconstant/polyMeshディレクトリ内のファイルです。これには、メッシュ間のインターフェースを定義するための情報が含まれていることがあります。
>- AMI (Arbitrary Mesh Interface) or GGI (General Grid Interface): 異なるメッシュ間でのデータ交換を可能にするインターフェースです。これらは、createPatchユーティリティを使用して設定されることが多いです。
>- setSetやsubsetMesh: 特定のセルやフェイスを選択し、新しいメッシュを作成するためのユーティリティです。これらは、オーバーセットメッシュの一部を形成するために使用されることがあります。
>- extrudeMesh: メッシュを押し出して新しいメッシュを生成するためのユーティリティです。これは、オーバーセットメッシュの一部として使用されることがあります。
>
>これらのファイルやユーティリティは、メッシュ間の相互作用やオーバーセットメッシュの動的な挙動を定義するために使用されます。AllrunやAllrun.preスクリプト内でこれらのユーティリティが実行されるコマンドが含まれていることがあります。

</details>

やはり一般論しか返してくれない．

### gpt-4

<details>
  <summary>クリックして展開/折りたたみ</summary>

>(`topoSetDict`をadd to chatして)
>@Codebase @OFRepo @OFDoc2312 @OFCodeWiki @OFTutoWiki @OFDocs @OFPrimer @OFNotes @OFHoltzman このtopoSetDictではメッシュにどんな変更を加えようとしているのか，できるだけ詳しく教えてください．
>
---

>このtopoSetDictファイルは、特定のメッシュ操作を定義しています。以下のステップに従ってメッシュに変更を加えます：
>
>1. c0という名前の新しいcellSetを作成し、点(0.001 0.001 0.001)を内部に含む領域のセルを選択します。
>2. c1という名前の新しいcellSetを作成し、c0セットに基づいてセルを選択します。
>3. c1セットを反転させ、c1に含まれないセルを選択します。
>4. boxという名前の新しいcellSetを作成し、c1セットに基づいてセルを選択します。
>5. boxセットをサブセットとして定義し、指定されたボックス(0.004 -0.001 -100)(0.006 0.003 100)内のセルを選択します。
>6. 最後に、boxセットを反転させ、ボックス内に含まれないセルを選択します。
>
>これらの操作は、メッシュ内の特定の領域を選択し、それらの領域に基づいて新しいセルセットを作成するために使用されます。これは、メッシュのリファインメント、メッシュの部分的な削除、または特定の解析領域の定義に役立ちます。

</details>

まずまずの説明．c0,c1,boxは何に対応する？実行後の`constant/polyMesh/sets`を見ると，セル数は
- c0: 625
- c1: 256
- box: 881
なので，こういうことか：

![not supported file format.](../pictures/rotatingSquare_topoSetDict.webp)

## subsetMesh

`runApplication subsetMesh box -patch hole -overwrite`の意味は？


### gpt-3.5 turbo

<details>
  <summary>クリックして展開/折りたたみ</summary>

>Allrun.pre
>```sh
>runApplication subsetMesh box -patch hole -overwrite
>```
>@Codebase @OFRepo @OFDoc2312 @OFCodeWiki @OFTutoWiki @OFDocs @OFPrimer @OFNotes @OFHoltzman この一行で，OpenFOAMは何をしようとしているのか，できるだけ詳しく説明してください．回答のために同一フォルダ内のsystem/blockMeshDict, system/topoSetDictも参照してください．

---

>このコマンドは、OpenFOAMのsubsetMeshユーティリティを使用して、特定のセルセットを抽出し、新しいメッシュを作成します。具体的には、topoSetDictファイルで定義されたboxセルセットを使用して、指定されたボックス領域内のセルを選択し、それらによって形成される境界パッチにholeという名前を割り当てます。そして、新しいメッシュを作成する際に、元のメッシュを上書きします。この操作は、メッシュの一部を取り除いたり、特定の解析領域を抽出するために使用されます。

</details>

意外と要領を得た回答だが，もう少し具体的に教えてほしいところ．

### gpt-4

<details>
  <summary>クリックして展開/折りたたみ</summary>

>Allrun.pre
>```sh
>runApplication subsetMesh box -patch hole -overwrite
>```
>@Codebase @OFRepo @OFDoc2312 @OFCodeWiki @OFTutoWiki @OFDocs @OFPrimer @OFNotes @OFHoltzman この一行で，OpenFOAMは何をしようとしているのか，できるだけ詳しく説明してください．回答のために同一フォルダ内のsystem/blockMeshDict, system/topoSetDictも参照してください．

---

>このコマンドsubsetMesh box -patch hole -overwriteは、OpenFOAMのsubsetMeshユーティリティを使用して、メッシュから特定のセルセットを抽出し、新しいメッシュを作成する操作を行います。具体的には、以下のステップを実行します：
>
>1. boxという名前のセルセットを使用して、元のメッシュからセルを選択します。このセルセットは、topoSetDictファイルで定義されており、特定のボックス領域内のセルを含んでいます。
>2. -patch holeオプションは、選択されたセルによって形成される新しいメッシュの境界パッチにholeという名前を割り当てます。これはblockMeshDictファイルで定義されたパッチの一つです。
>3. -overwriteオプションは、新しいメッシュを作成する際に、元のメッシュを上書きすることを指示します。
>
>この操作は、メッシュの一部を取り除いたり、特定の解析領域を抽出するために使用されます。例えば、オーバーセットメッシュの一部として、またはメッシュのリファインメントや境界条件の適用において利用されることがあります。

</details>

そんなに変わらんかった

## topoSet

```sh
runApplication -s box topoSet
```

topoSet２回目．この行いらなくない？コメントアウトしても結果の違いは判別できなかった．

### gpt-3.5 turbo

<details>
  <summary>クリックして展開/折りたたみ</summary>

>Allrun.pre
>```sh
>runApplication -s box topoSet
>```
>@Codebase @OFRepo @OFDoc2312 @OFCodeWiki @OFTutoWiki @OFDocs @OFPrimer @OFNotes @OFHoltzman このOpenFOAMスクリプトに現れる２回めのtopoSetコマンドでは，何をしようとしていますか？できるだけ詳しく教えてください．

---

>```
>box topoSet
>```
>
>このコマンドは、OpenFOAMのtopoSetユーティリティを使用して、"box"という名前のセルセットを作成します。このセルセットは、後続のシミュレーション手順で使用される可能性があります。

</details>


### gpt-4

<details>
  <summary>クリックして展開/折りたたみ</summary>

>Allrun.pre
>```sh
>runApplication -s box topoSet
>```
>@Codebase @OFRepo @OFDoc2312 @OFCodeWiki @OFTutoWiki @OFDocs @OFPrimer @OFNotes @OFHoltzman このOpenFOAMスクリプトに現れる２回めのtopoSetコマンドでは，何をしようとしていますか？できるだけ詳しく教えてください．

---

>OpenFOAMのtopoSetコマンドは、メッシュ内の特定の領域を選択するために使用されます。このコマンドはtopoSetDictという設定ファイルに基づいて動作し、そのファイル内で定義された基準に従ってセルや面を選択します。選択されたセルや面は、後で境界条件の設定やメッシュの操作などに使用されることがあります。1
>
>-s boxオプションは、topoSetDict内でboxという名前のセットを作成することを指示しています。このセットには、topoSetDictで定義された基準に一致するセルや面が含まれます。具体的な動作はtopoSetDictの内容に依存するため、詳細な動作を知るにはそのファイルを確認する必要があります。

</details>

gpt4でも引用していないファイルには言及できないっぽい？質問文で指定すればいけるかも

## restore0Dir

`0.org`から`0`フォルダを作成するだけ．

## setFields
runApplication setFields


### gpt-3.5 turbo

<details>
  <summary>クリックして展開/折りたたみ</summary>

使っていない

</details>


### gpt-4

<details>
  <summary>クリックして展開/折りたたみ</summary>

>(setFieldDictを全文add to chatして)
>@Codebase @OFRepo @OFDoc2312 @OFCodeWiki @OFTutoWiki @OFDocs @OFPrimer @OFNotes @OFHoltzman このsetFieldDictでは何をしようとしていますか？同一階層のtopoSetDictやblockMeshDictも参考に，できるだけ詳しく説明してください．

>---

>setFieldsDictファイルは、OpenFOAMのsetFieldsユーティリティによって使用され、シミュレーションの開始前に特定の領域に対してフィールド値を設定するためのものです。このファイルでは、defaultFieldValuesセクションでデフォルトのフィールド値を設定し、regionsセクションで特定のセルセットに対してフィールド値を変更しています。1
>
>具体的には、defaultFieldValuesでzoneIDというスカラーフィールドにデフォルト値123を設定し、regionsセクションではc0とc1という名前のセルセットに対してzoneIDの値をそれぞれ0と1に設定しています。これらのセルセットはtopoSetDictファイルで定義されており、blockMeshDictファイルで生成されたメッシュに対して適用されます。
>
>topoSetDictファイルでは、c0とc1というセルセットがどのように定義されているかを指定しており、blockMeshDictファイルはメッシュの構造を定義しています。したがって、setFieldsDictで指定されたフィールド値の変更は、これらのファイルで定義されたメッシュの特定の領域に適用されます。

</details>

