# Train passed by another

Author  : M.TANAKA
Created : 2024/01/08
Modified:

[TOC]

## 0. はじめに

このドキュメントは整理されていません．実際に取り組んだ時系列に沿って書かれています．

## 1. やりたいこと

### 1.1 きっかけ

- 新幹線に乗っていて対向車とすれ違ったとき，先頭が通過するとき押されて，中間車が通過するときは何もなく，最後尾が通過するとき引かれるように感じた．
- よく「自動車に乗っていてトラックとすれ違うときに引き寄せられるのは，自動車とトラックの狭い隙間に速い流れが生じるため（ベルヌーイの定理から流れが速いと圧力は低くなって云々）」と聞くが，あれが正しいなら中間車が通過するときも引かれるのではないか？正しい説明なのか？と思った次第
- 理論的な考察は別に考えるとして，数値解析で以下を確かめたくなった．
    - 車両がすれ違うとき，車両に生じる流体力の時間変化はどうなっているのか

### 1.2 計算対象

図のような長さ120m，幅26mの２次元の計算領域を考える．P0が静止している列車の中心，P1が追い越す列車の中心を表す．P1のx座標は計算の初期時刻t = 0sにおいてx = 20m，終了時刻t = 1sにおいてx = 100mである．したがって，追い越す列車の速度は80m/s = 288km/hである．列車はそれぞれ図のように長円で表され，進行方向に対する長さは20m，幅は4mである．線路は6m間隔で並んでいるものとする，つまり列車がすれ違うときの列車同士の隙間は2mである．

![](./pictures/computational_domain.webp)


## 2. ベースとなるtutorial

重合格子が必要そうなので，それっぽいケースを持ってきた．

- [rotatingSquare](https://develop.openfoam.com/Development/openfoam/-/tree/master/tutorials/incompressible/overPimpleDyMFoam/rotatingSquare)

もともとは回転するようなので，並進するように変更したい．

## 3. このtutorialの読み解き

初めてのことが多く，重合格子を用いた計算の操作に慣れる所から始めないといけなかった．

[こちらのファイル](./rotatingSquare/README.md)にまとめている．

## 4. tutorialの改変

### 4.1 回転運動を並進運動に変更

### 4.1 もう一つのboxを追加


## 4. 新tutorialについて質問

質問

>rotatingSquare/constant/dynamicMeshDict
>```
>multiSolidBodyMotionSolverCoeffs
>{
>    background
>    {
>        solidBodyMotionFunction linearMotion;
>        linearMotionCoeffs
>        {
>            velocity    (0 0 0);
>        }
>    }
>
>    movingZone
>    {
>        solidBodyMotionFunction rotatingMotion;
>        origin  (0.005 0 0.005);
>        axis    (0 0 1);
>        omega   20;
>    }
>}
>```
>ここでは２つのメッシュ"background"と"movingZone"が参照されているようです．これらのキーワードが指すメッシュたちは，どのファイルのどの箇所で定義されていますか？

回答
>backgroundとmovingZoneは、dynamicMeshDictファイル内で参照されているメッシュゾーンの名前です。これらのゾーンは、メッシュ内の異なる領域を指し、それぞれ異なる動的な動きを定義するために使用されます。
>
>これらのメッシュゾーンは、通常、cellZonesとしてblockMeshDictファイルやtopoSetDictファイルで定義され、その後createPatchやtopoSetなどのユーティリティを使用してconstant/polyMeshディレクトリ内のメッシュデータに組み込まれます。
>
>- blockMeshDict: このファイルは、メッシュの基本的な構造を定義します。blocksセクションでメッシュを定義した後、cellZonesセクションでメッシュ内の特定の領域をcellZoneとして指定することができます。
>
>- topoSetDict: このファイルは、特定の条件に基づいてメッシュ内のセルを選択し、それらをセットやゾーンにグループ化するために使用されます。topoSetユーティリティを実行することで、これらのセットやゾーンが作成されます。
>
>- createPatchDict: このファイルは、createPatchユーティリティを使用して新しいパッチを作成または既存のパッチを変更するための設定を含みます。これにより、メッシュの境界条件を設定することができます。
>
>dynamicMeshDictファイル内で指定された動的な動きは、これらのゾーンに適用されます。例えば、movingZoneゾーンは回転運動を行い、backgroundゾーンは線形運動（この場合は静止している）を行います。これらの設定は、ソルバーがメッシュを時間とともにどのように動かすかを決定するために使用されます。
>
>具体的なファイルの場所や設定の詳細は、使用しているOpenFOAMのバージョンや解析の詳細によって異なるため、各ケースのドキュメントや設定ファイルを参照する必要があります。また、OFUserGuide_oldやOF2312UserGuideなどのドキュメントには、これらのファイルの書き方や使用方法に関する情報が含まれています。

回答にはcodebaseも利用しているのに，曖昧な答え．．．
codebaseは，作業フォルダ全体を参照するものと思っていたが，違う？

## それっぽい回答の保存

正しいかは不明．

(質問)
OpenFOAMで移動物体周りの流れを計算したいです．具体的には，x方向に20m，y方向に10mある２次元空間を，半径1mの円柱がxの正方向に運動します．円柱の中心の座標は，時刻T=0.0sにおいて(5,5)，T=1.0sにおいて(15,5)です．このような運動を実現するblockMeshDict及びdynamicMeshDictを記述してください．

(回答)
OpenFOAMで移動する円柱周りの流れを計算するためには、blockMeshDictで基本的なメッシュを生成し、dynamicMeshDictで円柱の動きを定義します。以下は、指定された条件に基づいてこれらのファイルを記述する方法です。
blockMeshDict

まず、blockMeshDictを使用して、円柱を除いた2次元空間のメッシュを生成します。円柱は後でdynamicMeshユーティリティを使って動的に追加します。

```
/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://www.openfoam.com
    \\  /    A nd           | Version:  2.3.1
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      blockMeshDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

convertToMeters 1;

vertices
(
    (0 0 0)    // vertex 0
    (20 0 0)   // vertex 1
    (20 10 0)  // vertex 2
    (0 10 0)   // vertex 3
    (0 0 0.1)  // vertex 4, for 2D simulation z dimension is small
    (20 0 0.1) // vertex 5
    (20 10 0.1)// vertex 6
    (0 10 0.1) // vertex 7
);

blocks
(
    hex (0 1 2 3 4 5 6 7) (200 100 1) simpleGrading (1 1 1)
);

edges
(
    // No curved edges in the basic mesh
);

boundary
(
    inlet
    {
        type patch;
        faces
        (
            (0 4 7 3)
        );
    }
    outlet
    {
        type patch;
        faces
        (
            (1 5 6 2)
        );
    }
    bottom
    {
        type wall;
        faces
        (
            (0 1 5 4)
        );
    }
    top
    {
        type wall;
        faces
        (
            (2 6 7 3)
        );
    }
    frontAndBack
    {
        type empty;
        faces
        (
            (0 1 2 3)
            (4 5 6 7)
        );
    }
);

mergePatchPairs
(
    // Generally not used for 2D cases
);

// ************************************************************************* //
```

dynamicMeshDict

次に、dynamicMeshDictを使用して円柱の動きを定義します。OpenFOAMの標準ソルバーには動的メッシュを扱う機能がありますが、ここではdynamicRefineFvMeshを使用する例を示します。これは、メッシュの局所的な細分化と粗化を行うことができます。

```
dynamicFvMesh dynamicRefineFvMesh;

dynamicRefineFvMeshCoeffs
{
    // Refinement parameters here
}

motionSolverLibs ("libfvMotionSolver.so");

solidBodyMotionFunction oscillatingRotatingMotion;

oscillatingRotatingMotionCoeffs
{
    origin (5 5 0);  // Initial center of the cylinder
    axis (1 0 0);    // Axis of rotation (not used for translation)
    omega 0;         // Angular velocity (not used for translation)
    amplitude ((10 0 0)); // Translation amplitude
    period 1;        // Time period for one complete motion cycle
}
```
この設定では、円柱は原点(5, 5, 0)から始まり、x軸に沿って10メートル移動します。この動きは、oscillatingRotatingMotion関数を使用して定義されていますが、円柱の移動にはsolidBodyMotionFunctionを使用する必要があります。円柱の移動を実現するためには、dynamicMeshDictの設定を以下のように変更する必要があります。
```
dynamicFvMesh dynamicMotionSolverFvMesh;

dynamicMotionSolverFvMeshCoeffs
{
    motionSolverLibs ("libfvMotionSolver.so");

    solver displacementSBRStress;
    cellZone cylinder;
    cellZoneMotion solidBody;

    solidBodyMotionFunction linearMotion;
    linearMotionCoeffs
    {
        origin (5 5 0);  // 初期中心位置
        axis (1 0 0);    // 移動方向
        velocity (10 0 0); // 移動速度 m/s
    }
}
```
この設定では、cellZoneとしてcylinderを指定し、solidBodyMotionFunctionとしてlinearMotionを使用しています。linearMotionCoeffsで、円柱の初期位置、移動方向、および速度を定義しています。円柱の速度は、1秒間に10メートル移動することから、(10 0 0)と設定されています。

円柱のメッシュは、blockMeshとは別にsnappyHexMeshなどのユーティリティを使用して生成するか、またはtopoSetとcreateBafflesを使用して動的に生成する必要があります。これは、blockMeshDictではなく、別の設定ファイルで行われます。

最後に、blockMeshとsnappyHexMesh（または他のメッシュユーティリティ）を実行してメッシュを生成し、その後にソルバーを実行してシミュレーションを開始します。円柱の動きは、dynamicMeshDictによって制御されます。

円柱の動的な動きをシミュレーションするためには、適切なソルバーを選択することが重要です。例えば、pimpleDyMFoamやpisoDyMFoamなど、動的メッシュに対応したソルバーを使用する必要があります。また、円柱の動きに応じてメッシュが適切に更新されるように、ソルバーの設定を慎重に行う必要があります。