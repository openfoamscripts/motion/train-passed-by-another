# state file generated using paraview version 5.11.2
import paraview
paraview.compatibility.major = 5
paraview.compatibility.minor = 11

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1600, 450]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [60.95000076293945, 13.0, 0.5291760563850403]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [60.95000076293945, 13.0, 241.34617864839683]
renderView1.CameraFocalPoint = [60.95000076293945, 13.0, 0.5291760563850403]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 24.030152420341953
renderView1.CameraParallelProjection = 1

# init the 'GridAxes3DActor' selected for 'AxesGrid'
renderView1.AxesGrid.Visibility = 1
renderView1.AxesGrid.ShowGrid = 1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(1600, 450)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
step2_rotatingSquare_modfoam = OpenFOAMReader(registrationName='step2_rotatingSquare_mod.foam', FileName='/mnt/HDD1/GitLab/OF/motion/train-passed-by-another/step2_rotatingSquare_mod/step2_rotatingSquare_mod.foam')
step2_rotatingSquare_modfoam.MeshRegions = ['patch/inlet', 'patch/hole', 'patch/frontAndBack']
step2_rotatingSquare_modfoam.CellArrays = ['U', 'cellTypes', 'p', 'zoneID']

# create a new 'Threshold'
threshold1 = Threshold(registrationName='Threshold1', Input=step2_rotatingSquare_modfoam)
threshold1.Scalars = ['CELLS', 'cellTypes']
threshold1.LowerThreshold = 1.0
threshold1.UpperThreshold = 8534.0439453125
threshold1.ThresholdMethod = 'Below Lower Threshold'

# create a new 'Annotate Time Filter'
annotateTimeFilter1 = AnnotateTimeFilter(registrationName='AnnotateTimeFilter1', Input=step2_rotatingSquare_modfoam)

# create a new 'Glyph'
glyph1 = Glyph(registrationName='Glyph1', Input=step2_rotatingSquare_modfoam,
    GlyphType='Arrow')
glyph1.OrientationArray = ['CELLS', 'U']
glyph1.ScaleArray = ['CELLS', 'U']
glyph1.ScaleFactor = 0.03
glyph1.GlyphTransform = 'Transform2'
glyph1.MaximumNumberOfSamplePoints = 2500
glyph1.Seed = 2500

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from glyph1
glyph1Display = Show(glyph1, renderView1, 'GeometryRepresentation')

# get 2D transfer function for 'p'
pTF2D = GetTransferFunction2D('p')

# get color transfer function/color map for 'p'
pLUT = GetColorTransferFunction('p')
pLUT.TransferFunction2D = pTF2D
pLUT.RGBPoints = [-3739.951904296875, 0.231373, 0.298039, 0.752941, 2814.6461181640625, 0.865003, 0.865003, 0.865003, 9369.244140625, 0.705882, 0.0156863, 0.14902]
pLUT.ScalarRangeInitialized = 1.0

# trace defaults for the display properties.
glyph1Display.Representation = 'Surface'
glyph1Display.ColorArrayName = ['POINTS', '']
glyph1Display.LookupTable = pLUT
glyph1Display.SelectTCoordArray = 'None'
glyph1Display.SelectNormalArray = 'None'
glyph1Display.SelectTangentArray = 'None'
glyph1Display.OSPRayScaleArray = 'p'
glyph1Display.OSPRayScaleFunction = 'PiecewiseFunction'
glyph1Display.SelectOrientationVectors = 'U'
glyph1Display.ScaleFactor = 7780.224609375
glyph1Display.SelectScaleArray = 'p'
glyph1Display.GlyphType = 'Arrow'
glyph1Display.GlyphTableIndexArray = 'p'
glyph1Display.GaussianRadius = 389.01123046875
glyph1Display.SetScaleArray = ['POINTS', 'p']
glyph1Display.ScaleTransferFunction = 'PiecewiseFunction'
glyph1Display.OpacityArray = ['POINTS', 'p']
glyph1Display.OpacityTransferFunction = 'PiecewiseFunction'
glyph1Display.DataAxesGrid = 'GridAxesRepresentation'
glyph1Display.PolarAxes = 'PolarAxesRepresentation'
glyph1Display.SelectInputVectors = ['POINTS', 'U']
glyph1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
glyph1Display.ScaleTransferFunction.Points = [-2995.32763671875, 0.0, 0.5, 0.0, 3499.6220703125, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
glyph1Display.OpacityTransferFunction.Points = [-2995.32763671875, 0.0, 0.5, 0.0, 3499.6220703125, 1.0, 0.5, 0.0]

# show data from annotateTimeFilter1
annotateTimeFilter1Display = Show(annotateTimeFilter1, renderView1, 'TextSourceRepresentation')

# trace defaults for the display properties.
annotateTimeFilter1Display.FontSize = 32

# show data from threshold1
threshold1Display = Show(threshold1, renderView1, 'UnstructuredGridRepresentation')

# get 2D transfer function for 'U'
uTF2D = GetTransferFunction2D('U')
uTF2D.ScalarRangeInitialized = 1
uTF2D.Range = [0.0, 100.0, 0.0, 1.0]

# get color transfer function/color map for 'U'
uLUT = GetColorTransferFunction('U')
uLUT.AutomaticRescaleRangeMode = 'Never'
uLUT.TransferFunction2D = uTF2D
uLUT.RGBPoints = [0.0, 0.0, 0.0, 1.0, 100.0, 1.0, 0.0, 0.0]
uLUT.ColorSpace = 'HSV'
uLUT.NanColor = [0.498039215686, 0.498039215686, 0.498039215686]
uLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'U'
uPWF = GetOpacityTransferFunction('U')
uPWF.Points = [0.0, 0.0, 0.5, 0.0, 100.0, 1.0, 0.5, 0.0]
uPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
threshold1Display.Representation = 'Surface'
threshold1Display.ColorArrayName = ['CELLS', 'U']
threshold1Display.LookupTable = uLUT
threshold1Display.SelectTCoordArray = 'None'
threshold1Display.SelectNormalArray = 'None'
threshold1Display.SelectTangentArray = 'None'
threshold1Display.OSPRayScaleArray = 'p'
threshold1Display.OSPRayScaleFunction = 'PiecewiseFunction'
threshold1Display.SelectOrientationVectors = 'U'
threshold1Display.ScaleFactor = 12.0
threshold1Display.SelectScaleArray = 'p'
threshold1Display.GlyphType = 'Arrow'
threshold1Display.GlyphTableIndexArray = 'p'
threshold1Display.GaussianRadius = 0.6
threshold1Display.SetScaleArray = ['POINTS', 'p']
threshold1Display.ScaleTransferFunction = 'PiecewiseFunction'
threshold1Display.OpacityArray = ['POINTS', 'p']
threshold1Display.OpacityTransferFunction = 'PiecewiseFunction'
threshold1Display.DataAxesGrid = 'GridAxesRepresentation'
threshold1Display.PolarAxes = 'PolarAxesRepresentation'
threshold1Display.ScalarOpacityFunction = uPWF
threshold1Display.ScalarOpacityUnitDistance = 6.151865198589257
threshold1Display.OpacityArrayName = ['POINTS', 'p']
threshold1Display.SelectInputVectors = ['POINTS', 'U']
threshold1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
threshold1Display.ScaleTransferFunction.Points = [-2460.90087890625, 0.0, 0.5, 0.0, 8534.0439453125, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
threshold1Display.OpacityTransferFunction.Points = [-2460.90087890625, 0.0, 0.5, 0.0, 8534.0439453125, 1.0, 0.5, 0.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for uLUT in view renderView1
uLUTColorBar = GetScalarBar(uLUT, renderView1)
uLUTColorBar.Orientation = 'Horizontal'
uLUTColorBar.WindowLocation = 'Any Location'
uLUTColorBar.Position = [0.6387499999999996, 0.026455026455026454]
uLUTColorBar.Title = 'U'
uLUTColorBar.ComponentTitle = 'Magnitude'
uLUTColorBar.TitleFontSize = 24
uLUTColorBar.LabelFontSize = 24
uLUTColorBar.ScalarBarLength = 0.33000000000000007

# set color bar visibility
uLUTColorBar.Visibility = 1

# show color legend
threshold1Display.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get opacity transfer function/opacity map for 'p'
pPWF = GetOpacityTransferFunction('p')
pPWF.Points = [-3739.951904296875, 0.0, 0.5, 0.0, 9369.244140625, 1.0, 0.5, 0.0]
pPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# restore active source
SetActiveSource(threshold1)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')