# チュートリアルの改変

Author: M.TANAKA  
Created: 2024/01/20  
Modified: 2024/01/20  

[TOC]

## 概要

計算領域の形も変える．

なお，計算結果の可視化動画は[ここ](animation/)に保存している．

## 変更点

- system/controlDict
- system/blockMeshDict
- system/topoSetDict
- system/fvSchemes/oversetInterpolation.searchBox
- system/fvSchemes/wallDist
- system/fvSolution/relaxationFactors
- constant/dynamicMeshDict
- constant/turbulenceProperties
- 0.org/*

## 警告・エラー

<details>
  <summary>Removed group 'overset'</summary>

>log.overPimpleDyMFoam
>```
>--> FOAM Warning : 
>    From void Foam::polyBoundaryMesh::calcGroupIDs() const
>    in file meshes/polyMesh/polyBoundaryMesh/polyBoundaryMesh.C at line 98
>    Removed group 'overset' which clashes with patch 0 of the same name.
>```
>あなたはOpenFOAMの専門家です．この警告は何を意味していますか？解消するためにはどうすれば良いですか？

---

>この警告は、OpenFOAMが'overset'という名前のグループを削除したことを示しています。これは、同じ名前のパッチ0と衝突していたためです。
>
>OpenFOAMでは、グループとパッチは異なるエンティティであり、同じ名前を持つことはできません。グループは、複数のパッチをまとめて扱うためのもので、パッチは境界条件を適用するメッシュの面の集合を指します。
>
>この警告を解消するためには、'overset'という名前のグループまたはパッチの名前を変更する必要があります。どちらを変更するべきかは、具体的な問題設定や、どちらのエンティティが重要な役割を果たしているかによります。
>
>例えば、'overset'グループが特定の操作（例えば、特定の境界条件の適用）のために作成されたものであれば、そのグループの名前を変更することで警告を解消できます。一方、'overset'パッチが重要な役割を果たしている場合は、そのパッチの名前を変更することで警告を解消できます。
>
>名前の変更は、通常、メッシュを生成または編集する際に使用するユーティリティ（例えば、blockMeshやsnappyHexMesh）の辞書ファイルを編集することで行います。
  
</details>

