# チュートリアルの改変

Author: M.TANAKA  
Created: 2024/01/20  
Modified: 2024/01/20  

[TOC]

## 概要

すれ違う列車をイメージして，回転運動を並進運動に変える．ついでに物体の初期位置も変える．

なお，計算結果の可視化動画は[ここ](animation/)に保存している．

## 変更点

- system/blockMeshDict
- system/topoSetDict
- constant/dynamicMeshDict
